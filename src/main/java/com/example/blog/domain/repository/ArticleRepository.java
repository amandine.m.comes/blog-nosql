package com.example.blog.domain.repository;

import com.example.blog.domain.model.Article;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

public interface ArticleRepository extends MongoRepository <Article, String> {

    public Optional<Article> findByTitle(String title);
}
