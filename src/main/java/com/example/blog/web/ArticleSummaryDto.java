package com.example.blog.web;

public class ArticleSummaryDto {

    private String title;
    private String summary;
    private String id;

    public ArticleSummaryDto(String id, String title, String summary) {
        this.title = title;
        this.summary = summary;
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getSummary() {
        return summary;
    }

    public void setSummary(String summary) {
        this.summary = summary;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
}
