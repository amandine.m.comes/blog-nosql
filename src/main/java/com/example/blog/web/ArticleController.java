package com.example.blog.web;

import com.example.blog.application.ArticleService;
import com.example.blog.domain.model.Article;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import javax.websocket.server.PathParam;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@CrossOrigin(origins = "http://localhost:4200")
@RestController
@RequestMapping("/articles")
public class ArticleController {

    private final ArticleService articleService;

    @Autowired
    public ArticleController(ArticleService articleService) {
        this.articleService = articleService;
    }

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public Article createArticle(@RequestBody ArticleDetailDto articleDto){
        return this.articleService.createArticle(articleDto.getTitle(), articleDto.getSummary(), articleDto.getContent());
    }

    @GetMapping
    @ResponseStatus(HttpStatus.OK)
    public Iterable<ArticleSummaryDto> fetchArticles(){
        Iterable <Article> allArticles = this.articleService.getAllArticles();
        List<ArticleSummaryDto> articlesDtoFor = new ArrayList();
        for(Article article : allArticles) {
            articlesDtoFor.add(new ArticleSummaryDto(article.getId(), article.getTitle(), article.getSummary()));
        }
        return articlesDtoFor;

    }

    @GetMapping("/{articleId}")
    @ResponseStatus(HttpStatus.OK)
    public Optional<Article> findArticleById(@PathVariable("articleId") String articleId){
        return this.articleService.getArticle(articleId);
    }

    @GetMapping("/")
    @ResponseStatus(HttpStatus.OK)
    public Optional<Article> searchArticleByTitle(@RequestParam(name="title") String title){
        return this.articleService.getArticleByTitle(title);
    }


    @DeleteMapping("/{articleId}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void deleteArticle(@PathVariable("articleId") String articleId) {
        articleService.deleteArticle(articleId);
    }

    @PutMapping("/{articleId}")
    @ResponseStatus(HttpStatus.OK)
    public void changeArticleContent(@PathVariable("articleId") String articleId, @RequestBody ArticleDetailDto articleDetailDto) {
        articleService.changeArticleContent(articleId, articleDetailDto.getTitle(), articleDetailDto.getSummary(), articleDetailDto.getContent());
    }


}
