package com.example.blog.application;

import com.example.blog.domain.model.Article;
import com.example.blog.domain.repository.ArticleRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Component
public class ArticleService {

    private final ArticleRepository articleRepository;

    @Autowired
    public ArticleService(ArticleRepository articleRepository) {
        this.articleRepository = articleRepository;
    }


    public Article createArticle(String title, String summary, String content){
        Article article = new Article(UUID.randomUUID().toString(), title, summary, content);
        articleRepository.save(article);
        return article;
    }

    public List<Article> getAllArticles(){
        return articleRepository.findAll();
    }

    public Optional<Article> getArticle(String articleId){
        return articleRepository.findById(articleId);
    }

    public void deleteArticle(String articleId) {articleRepository.deleteById(articleId);}

    public void changeArticleContent(String articleId, String newTitle, String newSummary, String newContent ) {
        Optional<Article> optionalArticle = articleRepository.findById(articleId);
        Article article = optionalArticle.orElseThrow();
        article.setTitle(newTitle);
        article.setSummary(newSummary);
        article.setContent(newContent);
        articleRepository.save(article);
    }

    public Optional <Article> getArticleByTitle(String title) {return articleRepository.findByTitle(title);}
}

